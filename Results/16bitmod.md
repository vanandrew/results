#### >8-bit modifications to Progressive GAN code

This guide describes the necessary steps to convert the
[Progressive GAN code](https://github.com/tkarras/progressive_growing_of_gans)
to handle images beyond 8-bits (0-255). For convienence, I describe changes
for 16-bit images, but just replace the appropriate lines with the desired data
type.

We start with `dataset_tool.py`, where we want to modify the `class TFRecordExporter`
to save out our image conversions to the new data type. Under the `add_image` method,
modify the following line:

```
...
quant = np.rint(img).clip(0, 65535).astype(np.uint16)   # I choose uint16 dynamic range and data type
...
```

This will ensure that the `*.tfrecords` files will saved with the new data type and range. Note that
you still need to ensure that the input images you are feeding in are already in the new data type and 
range (for example, the input images I am using are 16-bit png files that range from 0-65535 in intensity).

Next, we go to `dataset.py`. Under `class TFRecordDataset` change the `dtype`
and `dynamic_range` attributes:

```
...
self.dtype              = 'uint16'      # set to unsigned 16 bit integer
self.dynamic_range      = [0, 65535]    # set the dynamic range to uint16 range 
...
```

You will also need to change the following lines under `parse_tfrecord_tf` and
`parse_tfrecord_np`:

```
...
data = tf.decode_raw(features['data'], tf.uint16)
...
...
return np.fromstring(data, np.uint16).reshape(shape)
...

```

This will ensure that the code loads the dataset with the appropriate data type.

Now we go to `misc.py`, where we want to modify the `convert_to_pil_image` function.
This will make the code save out our fake generator images using the new data type.
Unfortunately, the PIL library doesn't seem to handle 16-bit images very well... so I
had to do a workaround by converting the image to a bytes representation before
writing it to the image container.

```
...
image = adjust_dynamic_range(image, drange, [0,65535])
image = np.rint(image).clip(0, 65535).astype(np.uint16)

#format = 'RGB' if image.ndim == 3 else 'L'     # <-- comment these lines
#return PIL.Image.fromarray(image, format)      # <-- to disable 8-bit write out

# Custom code for saving in 16 bit
img = PIL.Image.new("I",image.T.shape)          # <-- this will change if you use
img.frombytes(image.tobytes(),'raw',"I;16")     # <-- a different data type
return img
...
```

If 16-bit is not your goal, you will need to read the [Pillow Docs](https://pillow.readthedocs.io/)
(or google) for further details.

The final modification is in `util_scripts.py`, where you will need to modify
the `generate_fake_images` function (and optionally the `generate_interpolation_video`
function):

```
...
images = Gs.run(latents, labels, minibatch_size=minibatch_size, num_gpus=config.num_gpus, out_mul=32767.5, out_add=32767.5, out_shrink=image_shrink, out_dtype=np.uint16)
misc.save_image_grid(images, os.path.join(result_subdir, '%s%06d.png' % (png_prefix, png_idx)), [0,65535], grid_size)
...
```

```
...
"""
    This is optional. I don't do this because I don't think the video encoder can handle
    16-bit without some extra compilation flags... You are free to try though.
"""
# under the inner `make_frame` function
images = Gs.run(latents, labels, minibatch_size=minibatch_size, num_gpus=config.num_gpus, out_mul=32767.5, out_add=32767.5, out_shrink=image_shrink, out_dtype=np.uint16)
...
```

And that's it. Let me know if you see any errors and I will update this guide.