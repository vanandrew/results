### Problem Formulation

Let $`x \in \mathbb{R}^{N}`$ be a T1/T2 image, and $`y \in \mathbb{R}^{M}`$ ($`M < N`$)
be a functional image. The objective is to find a mapping that transforms the
T1/T2 image into an epi image.

Given that the T1/T2 image can inform us about the contents of the epi image, this can be posed
similarly to a super-resolution reconstruction problem. However, rather than trying to
reconstruct the object, the objective to uncover an accurate mapping between the functional
and structural images. We first restate the super-resolution problem which is given by:

```math
x_{LR} = \mathbf{D}\mathbf{F}\mathbf{M}x_{HR} + \mathbf{n}
```

where $`\mathbf{M}`$ is a linear operator describing the motion relative to the T1/T2
frame of reference, $`\mathbf{F}`$  is a linear system operator describing the
imaging system, $`\mathbf{D}`$ is a downsampling operator, and $`\mathbf{n}`$ is
additive noise.

We modify the above equation by replacing the the linear system operator with some arbitrary 
function (given that the relation between the T1/T2 images and the epi image can be, and is
most likely non-linear):

```math
y = \mathbf{D}\mathbf{M}f(x;\theta) + \mathbf{n}
```

where $`f: \mathbb{R}^{N} \rightarrow \mathbb{R}^{N}`$ parameterized by $`\theta`$,
is a map from the T1/T2 image to the an upscaled version of the epi image
before the downsampling operation, $`\mathbf{D}`$.

From the above equation, we would like to find the optimal $`\mathbf{M}`$ and $`\theta`$.
We immediately note that this will require solving a joint non-linear optimization problem.

### Proposed Solution

We "linearize" the system by applying a linear operator to a non-linear transform of the inputs.

```math
\mathbf{D}^{-1}y = \mathbf{M}\theta z + \mathbf{n}
```

where $`\theta`$ is a matrix of coefficients to be optimized, and $`z`$ is the result of a non-linear
transform of $`x`$. The motion matrix, $`\mathbf{M}`$, can be parameterized by a set of affine coefficients
$`\gamma`$.

The objective function can be constructed as a least squares problem, given by:

```math
\min_{\theta,\gamma} \frac{1}{2} \lVert \mathbf{M}\theta z - \mathbf{D}^{-1}y \rVert^{2}
```

In order to minimize this objective function, we first solve for the gradient with
respect to $`\theta`$ $`\gamma`$.

##### Gradient with respect to $`\theta`$

